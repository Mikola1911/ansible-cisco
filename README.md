1. Установить ansible на мастер хост, на slave должен быть python
2. Пробросить SSH-ключи на настраиваемые хосты (ssh-keygen на мастер ноде, ssh-copy-id user@10.0.2.15 - прокинуть на другие хосты ключи)
3. Склоировать репозиторий на мастер хост git clone https://gitlab.com/Mikola1911/ansible-cisco.git
4. Настроить файл hosts в директории cisco
5. Выполнить ansible-playbook -i ~/cisco/hosts (путь к файлу hosts) ~/cisco/playbook.yml (путь к playbook.yml)
